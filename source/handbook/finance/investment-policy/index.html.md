---
layout: markdown_page
title: "Investment Policy"
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

## Investment Policy

 
#### Purpose
The purpose of this policy is to establish the responsibility, authority and guidelines for the investment of operating surplus cash.  Surplus cash is defined as those funds exceeding the operating requirements of the Company and not immediately required for working capital or near term financial obligations.

#### Scope
This policy shall apply to the Company and all subsidiaries.  This investment policy will be reviewed periodically to ensure that it remains consistent with the overall objectives of the Company and with current financial trends.

#### Approved Brokerage Institutions
The Company may use the following brokerage institutions:

1. Morgan Stanley Smith Barney LLC
1. Comerica Securities, Inc.

#### Investment Objectives 

The basic objectives of the Company’s investment program are, in order of priority:
Safety and preservation of principal by investing in a high quality, diversified portfolio of securities, mutual funds, and bank deposits.
Liquidity of investments that is sufficient to meet the Company’s projected cash flow requirements and strategic needs.
Maximize after-tax market rates of return on invested funds that are consistent with the stated objectives herein, conservative risk tolerance and the Company’s current tax position.
Maturity Limits
Individual security maturities should not exceed 24 months.  The weighted average maturity of the portfolio shall not exceed 12 months.  A maturity or effective maturity by definition shall include puts, announced calls or other structural features which will allow the Company to redeem the investments at a quantifiable price consistent with liquidity, safety and preservation of capital.
 
#### Eligible Investments
United States Government Securities:
Marketable debt securities which are direct obligations of the U.S.A., issued by or guaranteed as to principal and interest by the U.S. Government and supported by the full faith and credit of the United States.
United States Government Agency Securities:
Debt securities issued by the Government Sponsored Enterprises, Federal Agencies and certain international institutions which are not direct obligations of the United States, but involve Government sponsorship and are fully guaranteed by government agencies or enterprises, including but not limited to:
·   	Federal Farm Credit Bank (FFCB)
·   	Federal Home Loan Bank (FHLB)
·   	Federal Home Loan Mortgage Corporation (FHLMC)
·   	Federal National Mortgage Association (FNMA)
 
 
### Money Market funds
 
Money Market Funds must be rated AAA or equivalent by at least one NRSROs.
 
 
At time of purchase investment restrictions:

Investment Products (Rating, Sector Concentration, Issuer Concentration)
 
1. US Government (AA+, No Sector Concentration, No Issue Concentration)
1. Agency (AA+, 50% Sector Concentration, 10% Issuer Concentration)
1. Money Market Funds - US Government/Treasury (AAA, No Sector Concentration, 50% Issuer Concentration)
