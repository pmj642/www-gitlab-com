/* eslint-disable no-unused-vars */

function setupCountdown(countDownDate, countdownElementId) {
  var x = setInterval(function() {
    var now = new Date().getTime();

    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    document.getElementById(countdownElementId).innerHTML = days + ' days ' + hours + ' hours '
      + minutes + ' minutes ' + seconds + ' seconds ';

    // If the count down is over, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById(countdownElementId).innerHTML = 'Already happened!';
    }
  }, 1000);
}
